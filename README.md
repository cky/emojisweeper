## A Minesweeper game with more emoji 😉

Slides: [EmojiSweeper: 💣💥 with Reagent and re-frame][slides]

Right now you can play the standard Minesweeper large board (30×16),
with other board sizes to be supported later.

## How to play

The point of the game is to clear the board without triggering a mine.
When you left-click on a square, the spot you clicked will open up and
you will see numbers on some of the opened squares. The number tells
you how many of that square's neighbours are mines.

You can right-click on a square to flag it as a mine, so that you can
avoid accidentally triggering it.

When you have flagged the correct number of squares around a number (for
example, you've put 3 flags around a square labelled 3), you can click
on that number (with either mouse button) to open all the other squares
around it. This is a great way to speed up gameplay, with the risk of
instant death if you flagged the wrong squares.

To restart the game, left-click on the big smiley button. You can also
right-click it to change emoji style; currently [Twemoji], [EmojiOne],
and [Noto Emoji] are available.

To pause the game, click on the timer. When paused, the game grid will
be blanked out. Taking screenshots before pausing the game is considered
cheating. 😉

You can also toggle quick-flag mode by clicking on the mine/flag counter.
When quick-flag mode is activated, the left and right clicks are swapped
once the game has started; this allows you to flag quickly when playing
on devices where right-click is onerous, such as phones or MacBooks.

[MinesweeperWiki] has excellent strategies and resources you can learn
to optimise your Minesweeper gameplay.

## Why another Minesweeper?

I'm trying to teach myself [React], a much more consistent and
performant way to code frontends. I love playing Minesweeper, so I
thought this project is a great way to learn all the React techniques.

The original version was written in [CoffeeScript][emo-coffee], which I
ported over to [ECMAScript 2015][emo-babel]. Then I decided to see what
a ClojureScript version would look like. Having played around with
[Reagent] and [re-frame], I'm loving this style of frontend development
more and more. 😀

Another reason I created this was that there are so many Minesweeper
clones out there that try to preserve the retro pixelated look, and I
wanted to do something different. The ultimate break from pixelation
is to use vector graphics, of course. I don't know how to draw, so I
decided to use what's already around on many platforms: emoji!

## Author

[Chris Jester-Young]

[slides]: https://speakerdeck.com/cky/emojisweeper-with-reagent-and-re-frame
[Twemoji]: https://twitter.github.io/twemoji/
[EmojiOne]: http://emojione.com/
[Noto Emoji]: https://github.com/googlei18n/noto-emoji/
[MinesweeperWiki]: http://www.minesweeper.info/wiki/
[React]: https://facebook.github.io/react/
[emo-coffee]: https://codepen.io/cky/full/122e7dcea92531ccf8289e04a3320604
[emo-babel]: https://codepen.io/cky/full/f5923696f12019ed15622f7f24913929
[Reagent]: https://reagent-project.github.io/
[re-frame]: https://github.com/Day8/re-frame
[Chris Jester-Young]: http://about.cky.nz/
