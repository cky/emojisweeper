;;; EmojiSweeper, a Minesweeper game with more emoji 😉
;;; Copyright © 2016 Chris Jester-Young
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program. If not, see <http://www.gnu.org/licenses/>.

(ns emojisweeper.views
  (:require [re-frame.core :refer [dispatch subscribe]]
            [emojisweeper.util :refer [class-names]]))

(defn- pd-dispatch [v]
  (fn [e]
     (.preventDefault e)
     (dispatch v)))

(defn square [{:keys [open flag wrong label hide show
                      on-click on-mouse-up on-context-menu]}]
  (let [props {:class (if hide
                        nil
                        (class-names {:open open
                                      :flag flag
                                      :wrong wrong
                                      label (or show open)}))
               :disabled (or hide show)
               :on-click on-click
               :on-mouse-up on-mouse-up
               :on-context-menu on-context-menu}]
    [:button props
     [:div {:class "emoji"}]]))

(defn grid []
  (let [grid-state (subscribe [:grid-state])]
    (fn []
      (let [{:keys [neighbour-counts mine-set open-set flag-set
                    width height paused done]} @grid-state]
        [:div {:class (class-names :grid {:started mine-set
                                          :done done
                                          :paused paused})
               :style {:width (* width 32)}}
         (map #(let [open (open-set %)
                     flag (flag-set %)
                     mine (and mine-set (mine-set %))
                     nc (and neighbour-counts (neighbour-counts %))]
                 [square {:key %
                          :open open
                          :flag flag
                          :wrong (and done flag (not mine))
                          :label (if mine "mine" (str "clear" nc))
                          :hide paused
                          :show done
                          :on-click (if-not open (pd-dispatch [:open-square %]))
                          :on-mouse-up (if open (pd-dispatch [:clear-around %]))
                          :on-context-menu (if open
                                             (pd-dispatch [:clear-around %])
                                             (pd-dispatch [:flag-square %]))}])
              (range (* width height)))]))))

(defn smiley []
  (let [smiley-state (subscribe [:smiley-state])]
    (fn []
      (let [{:keys [won lost noflags paused]} @smiley-state]
        [:div {:class "smiley"}
         [:button {:class (class-names {:won (and won (not noflags))
                                        :lost lost
                                        :noflags (and won noflags)
                                        :paused paused})
                   :on-click (pd-dispatch [:restart-game])
                   :on-context-menu (pd-dispatch [:toggle-emoji-style])}
          [:div {:class "emoji"}]]]))))

(defn- emoji-digits [value width]
  (let [values (->> [0 0 value]
                    (iterate (fn [[i digit value]]
                               [(inc i) (rem value 10) (quot value 10)]))
                    rest
                    (take width)
                    reverse)]
    (if (-> values first peek zero?)
      (map (fn [[i digit]] [:span {:key i :class (str "digit digit" digit)}]) values)
      (cons [:span {:key width :class "digit digit10"}] (emoji-digits 0 (dec width))))))

(defn emoji-number [{:keys [class value width on-click]}]
  [:div {:class class :title value :on-click on-click} (emoji-digits value width)])

(defn timer []
  (let [timer-state (subscribe [:timer-state])]
    (fn []
      (let [{:keys [elapsed playing paused]} @timer-state]
        [emoji-number {:class (class-names :timer {:playing playing} {:paused paused})
                       :value elapsed
                       :width 3
                       :on-click (cond paused (pd-dispatch [:unpause-game])
                                       playing (pd-dispatch [:pause-game]))}]))))

(defn flags []
  (let [flags-state (subscribe [:flags-state])]
    (fn []
      (let [{:keys [flags-left]} @flags-state]
        [emoji-number {:class :flags
                       :value (max 0 flags-left)
                       :width 2
                       :on-click (pd-dispatch [:toggle-click-mode])}]))))

(defn sweeper []
  (let [app-state (subscribe [:app-state])]
    (fn []
      (let [{:keys [click-mode emoji-style]} @app-state]
        [:div {:class "sweeper"
               :data-click-mode click-mode
               :data-emoji-style emoji-style}
         [grid]
         [:div {:class "panel"}
          [smiley]
          [:div {:class "stats"}
           [timer]
           [flags]]]
         [:div {:class "preload1"}]
         [:div {:class "preload2"}]]))))
