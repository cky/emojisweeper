;;; EmojiSweeper, a Minesweeper game with more emoji 😉
;;; Copyright © 2016 Chris Jester-Young
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program. If not, see <http://www.gnu.org/licenses/>.

(ns emojisweeper.pcg
  (:refer-clojure :exclude [shuffle])
  (:import goog.math.Long))

(def ^:private default-multiplier (Long. 1284865837 1481765933))
(def ^:private default-increment (Long. -144211633 335903614))

(defn generate-seed []
  (let [values (js/Int32Array. 2)]
    (.getRandomValues js/crypto values)
    (Long. (aget values 0) (aget values 1))))

(defn- bit-rotate-right [x n]
  (bit-or (unsigned-bit-shift-right x n)
          (bit-shift-left x (- n))))

(defn- unsigned [x]
  (aget (.of js/Uint32Array x) 0))

;;; The following functions are based on the PCG reference implementation.
;;; Copyright © 2014 Melissa O'Neill <oneill@pcg-random.org>

(defn make-pcg32-seq [seed seqid]
  (let [state (or seed (generate-seed))
        increment (if seqid
                    (.or (.shiftLeft seqid 1) (.getOne Long))
                    default-increment)]
    (map #(-> %
              (.shiftRightUnsigned 18)
              (.xor %)
              (.shiftRightUnsigned 27)
              .toInt
              (bit-rotate-right (.toInt (.shiftRightUnsigned % 59)))
              unsigned)
         (iterate #(-> %
                       (.multiply default-multiplier)
                       (.add increment))
                  state))))

(defn shrinking-bounded-random-seq [random-seq bound]
  (if (zero? bound)
    ()
    (lazy-seq (let [threshold (js-mod (unsigned (- bound)) bound)]
                (loop [rs random-seq]
                  (if (>= (first rs) threshold)
                    (cons [bound (js-mod (first rs) bound) (next rs)]
                          (shrinking-bounded-random-seq (next rs) (dec bound)))
                    (recur (next rs))))))))

(defn shuffle [items random-seq]
  (let [v (vec items)
        [trans rs] (reduce (fn [[trans _] [i j rs]]
                             (let [value (trans j)]
                               [(-> trans
                                    (assoc! j (trans (dec i)))
                                    (assoc! (dec i) value))
                                rs]))
                           [(transient v) random-seq]
                           (shrinking-bounded-random-seq random-seq (count v)))]
    [(persistent! trans) rs]))
