(ns emojisweeper.core
  (:require [emojisweeper.handlers]
            [emojisweeper.subs]
            [emojisweeper.views :as views]
            [reagent.dom :as dom]
            [re-frame.core :refer [dispatch-sync]]))

;; -------------------------
;; Views

(defn home-page []
  [views/sweeper])

;; -------------------------
;; Initialize app

(defn mount-root []
  (dispatch-sync [:initialise-db])
  (dom/render [home-page] (.getElementById js/document "app")))

(defn init! []
  (mount-root))
