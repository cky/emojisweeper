;;; EmojiSweeper, a Minesweeper game with more emoji 😉
;;; Copyright © 2016 Chris Jester-Young
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program. If not, see <http://www.gnu.org/licenses/>.

(ns emojisweeper.handlers
  (:require [emojisweeper.db :as db]
            [emojisweeper.util :refer [for-each-neighbour]]
            [re-frame.core :refer [dispatch register-handler after trim-v]]
            [reagent.core :as reagent]))

(defn- start-tick []
  (reagent/next-tick #(dispatch [:tick])))

(def ^:private validate-schema-mw (after db/validator))

(register-handler :initialise-db
                  validate-schema-mw
                  #(db/initialise-db {:width 30 :height 16 :mines 100}))

(register-handler :restart-game
                  [validate-schema-mw trim-v]
                  (fn [{{:keys [width height mines]} :grid
                        {:keys [click-mode emoji-style]} :prefs}
                       {:keys [random-seed]}]
                    (db/initialise-db {:random-seed random-seed
                                       :width width :height height :mines mines
                                       :click-mode click-mode
                                       :emoji-style emoji-style})))

(register-handler :tick
                  validate-schema-mw
                  (fn [db]
                    (if (or (not (db/playing? db)) (db/paused? db))
                      db
                      (do
                        (start-tick)
                        (assoc-in db [:timer :current] (.now js/Date))))))

(register-handler :pause-game
                  validate-schema-mw
                  (fn [db]
                    (if (db/playing? db)
                      (assoc db :timer {:start nil
                                        :current nil
                                        :penalty (db/elapsed-ms db)
                                        :paused true})
                      db)))

(register-handler :unpause-game
                  validate-schema-mw
                  (fn [db]
                    (if (db/playing? db)
                      (do
                        (start-tick)
                        (assoc db :timer {:start (.now js/Date)
                                          :current (.now js/Date)
                                          :penalty (db/elapsed-ms db)
                                          :paused false}))
                      db)))

(defn- open-square [db i force-recurse]
  (if (and (not force-recurse)
           (or (db/open? db i) (db/flagged? db i)))
    db
    (let [db (update-in db [:state :open-set] conj i)
          clear-around (or force-recurse (db/clear? db i))]
      (cond (db/has-mine? db i) (assoc-in db [:state :lost] true)
            (db/opened-all? db) (assoc-in db [:state :won] true)
            clear-around (let [{:keys [width height]} (db/width-height db)]
                           (for-each-neighbour open-square width height db i))
            :else db))))

(defn- open-square-new-game [db i]
  (dispatch [:unpause-game])
  (open-square (db/generate-grid db i) i true))

(defn- flag-square [db i]
  (cond (db/open? db i) db
        (db/flagged? db i) (update-in db [:state :flag-set] disj i)
        :else (-> db
                  (update-in [:state :flag-set] conj i)
                  (assoc-in [:state :noflags] false))))

(register-handler :open-square
                  [validate-schema-mw trim-v]
                  (fn [db [i]]
                    (cond (db/done? db) db
                          (not (db/started? db)) (open-square-new-game db i)
                          (= (db/click-mode db) :flag) (flag-square db i)
                          :else (open-square db i false))))

(register-handler :flag-square
                  [validate-schema-mw trim-v]
                  (fn [db [i]]
                    (cond (db/done? db) db
                          (= (db/click-mode db) :flag) (if (db/started? db)
                                                         (open-square db i false)
                                                         (open-square-new-game db i))
                          :else (flag-square db i))))

(register-handler :clear-around
                  [validate-schema-mw trim-v]
                  (fn [db [i]]
                    (let [{:keys [width height]} (db/width-height db)
                          flag-count (for-each-neighbour inc width height 0 i
                                                         :filter (partial db/flagged? db))]
                      (if (= flag-count (db/neighbour-count db i))
                        (open-square db i true)
                        db))))

(register-handler :toggle-click-mode
                  validate-schema-mw
                  (fn [db]
                    (update-in db [:prefs :click-mode] #(case % :open :flag :open))))

(register-handler :toggle-emoji-style
                  validate-schema-mw
                  (fn [db]
                    (update-in db [:prefs :emoji-style] #(case % :twemoji :emojione :emojione :noto :twemoji))))
