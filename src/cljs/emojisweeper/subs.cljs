;;; EmojiSweeper, a Minesweeper game with more emoji 😉
;;; Copyright © 2016 Chris Jester-Young
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program. If not, see <http://www.gnu.org/licenses/>.

(ns emojisweeper.subs
  (:require [emojisweeper.db :as db]
            [reagent.ratom :refer-macros [reaction]]
            [re-frame.core :refer [register-sub subscribe]]))

(register-sub :app-state
              (fn [db]
                (reaction (:prefs @db))))

(register-sub :grid-state
              (fn [db]
                (let [grid (reaction (:grid @db))
                      state (reaction (:state @db))
                      paused (reaction (get-in @db [:timer :paused]))]
                  (reaction (let [{:keys [width height neighbour-counts mine-set]} @grid
                                  {:keys [open-set flag-set won lost]} @state]
                              {:width width
                               :height height
                               :neighbour-counts neighbour-counts
                               :mine-set mine-set
                               :open-set open-set
                               :flag-set flag-set
                               :paused @paused
                               :done (or won lost)})))))

(register-sub :smiley-state
              (fn [db]
                (let [state (reaction (:state @db))
                      paused (reaction (get-in @db [:timer :paused]))]
                  (reaction (let [{:keys [won lost noflags]} @state]
                              {:paused @paused
                               :won won
                               :lost lost
                               :noflags noflags})))))

(register-sub :timer-state
              (fn [db]
                (reaction {:elapsed (.floor js/Math (/ (db/elapsed-ms @db) 1000))
                           :playing (db/playing? @db)
                           :paused (db/paused? @db)})))

(register-sub :flags-state
              (fn [db]
                (reaction {:flags-left (db/flags-left @db)})))
