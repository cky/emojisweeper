;;; EmojiSweeper, a Minesweeper game with more emoji 😉
;;; Copyright © 2016 Chris Jester-Young
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program. If not, see <http://www.gnu.org/licenses/>.

(ns emojisweeper.db
  (:require [emojisweeper.util :refer [for-each-neighbour]]
            [emojisweeper.pcg :as pcg]
            [schema.core :as s :include-macros true])
  (:import goog.math.Long))

(def ^:private schema
  {:app {:random-seed Long}
   :grid {:width s/Int
          :height s/Int
          :mines s/Int
          :neighbour-counts (s/maybe {s/Int s/Int})
          :mine-set (s/maybe #{s/Int})}
   :state {:open-set #{s/Int}
           :flag-set #{s/Int}
           :won s/Bool
           :lost s/Bool
           :noflags s/Bool}
   :timer {:start (s/maybe s/Int)
           :current (s/maybe s/Int)
           :penalty s/Int
           :paused s/Bool}
   :prefs {:click-mode (s/enum :open :flag)
           :emoji-style (s/enum :twemoji :emojione :noto)}})

(def validator (s/validator schema))

(defn generate-grid [db opening-square]
  (let [{{:keys [random-seed]} :app
         {:keys [width height mines]} :grid} db
        grid-size (* width height)
        safe-squares (for-each-neighbour conj width height
                                         #{opening-square} opening-square)
        [shuffled _] (pcg/shuffle (concat (repeat (- grid-size (count safe-squares) mines) false)
                                          (repeat mines true))
                                  (pcg/make-pcg32-seq random-seed nil))]
    (loop [i 0
           neighbour-counts {}
           mine-set #{}
           shuffled shuffled]
      (cond (empty? shuffled) (update db :grid merge {:neighbour-counts neighbour-counts
                                                      :mine-set mine-set})
            (safe-squares i) (recur (inc i) neighbour-counts mine-set shuffled)
            (not (first shuffled)) (recur (inc i) neighbour-counts mine-set
                                          (rest shuffled))
            :else (recur (inc i)
                         (for-each-neighbour #(update %1 %2 inc)
                                             width height neighbour-counts i)
                         (conj mine-set i)
                         (rest shuffled))))))

(defn initialise-db [{:keys [random-seed width height mines click-mode emoji-style]}]
  {:app {:random-seed (or random-seed (pcg/generate-seed))}
   :grid {:width width
          :height height
          :mines mines
          :neighbour-counts nil
          :mine-set nil}
   :state {:open-set #{}
           :flag-set #{}
           :won false
           :lost false
           :noflags true}
   :timer {:start nil
           :current nil
           :penalty 0
           :paused false}
   :prefs {:click-mode (or click-mode :open)
           :emoji-style (or emoji-style :twemoji)}})

(defn started? [{{:keys [mine-set]} :grid}]
  mine-set)

(defn paused? [{{:keys [paused]} :timer}]
  paused)

(defn done? [{{:keys [won lost]} :state}]
  (or won lost))

(defn playing? [db]
  (and (started? db) (not (done? db))))

(defn opened-all? [{{:keys [width height mines]} :grid
                    {:keys [open-set]} :state}]
  (= (- (* width height) mines) (count open-set)))

(defn has-mine? [{{:keys [mine-set]} :grid} i]
  (mine-set i))

(defn neighbour-count [{{:keys [neighbour-counts]} :grid} i]
  (neighbour-counts i))

(defn clear? [db i]
  (not (neighbour-count db i)))

(defn open? [{{:keys [open-set]} :state} i]
  (open-set i))

(defn flagged? [{{:keys [flag-set]} :state} i]
  (flag-set i))

(defn elapsed-ms [{{:keys [start current penalty]} :timer}]
  (+ (- current start) penalty))

(defn flags-left [{{:keys [mines]} :grid {:keys [flag-set]} :state}]
  (max 0 (- mines (count flag-set))))

(defn width-height [{{:keys [width height]} :grid}]
  {:width width :height height})

(defn grid-size [{{:keys [width height]} :grid}]
  (* width height))

(defn click-mode [{{:keys [click-mode]} :prefs}]
  click-mode)

(defn emoji-style [{{:keys [emoji-style]} :prefs}]
  emoji-style)
