;;; EmojiSweeper, a Minesweeper game with more emoji 😉
;;; Copyright © 2016 Chris Jester-Young
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program. If not, see <http://www.gnu.org/licenses/>.

(ns emojisweeper.util
  (:require [clojure.string :refer [join]]))

(defn for-each-neighbour [callback width height state i & {:keys [filter]}]
  (let [row (quot i width)
        col (rem i width)
        call-if-in-range (fn [state row col]
                           (let [j (+ (* row width) col)]
                             (if (and (< -1 row height) (< -1 col width)
                                      (or (not filter) (filter j)))
                               (callback state j)
                               state)))]
    (-> state
        (call-if-in-range (dec row) (dec col))
        (call-if-in-range (dec row) col)
        (call-if-in-range (dec row) (inc col))
        (call-if-in-range row (dec col))
        (call-if-in-range row (inc col))
        (call-if-in-range (inc row) (dec col))
        (call-if-in-range (inc row) col)
        (call-if-in-range (inc row) (inc col)))))

(defn class-names [& arguments]
  (letfn [(process [arg]
            (cond (string? arg) arg
                  (number? arg) (str arg)
                  (array? arg) (join " " (map process arg))
                  (object? arg) (join " " (filter #(aget arg %) (.keys js/Object arg)))))]
    (process (clj->js arguments))))
