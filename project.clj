(defproject emojisweeper "0.1.0-SNAPSHOT"
  :description "A Minesweeper game with more emoji 😉"
  :url "http://www.emojisweeper.com/"
  :license {:name "GNU General Public License"
            :url "http://www.gnu.org/licenses/gpl-3.0.html"}

  :dependencies [[org.clojure/clojure "1.9.0-alpha10" :scope "provided"]
                 [org.clojure/clojurescript "1.9.93" :scope "provided"]
                 [org.slf4j/slf4j-nop "1.7.21" :scope "test"]
                 [cljsjs/react "15.3.0-0"]
                 [cljsjs/react-dom "15.3.0-0"]
                 [cljsjs/react-dom-server "15.3.0-0"]
                 [reagent "0.6.0-rc" :exclusions [cljsjs/react cljsjs/react-dom
                                                  cljsjs/react-dom-server]]
                 [re-frame "0.8.0-alpha9" :exclusions [reagent]]
                 [prismatic/schema "1.1.3"]]

  :plugins [[lein-cljsbuild "1.1.3"]
            [deraen/lein-sass4clj "0.2.1"]]

  :min-lein-version "2.5.0"

  :clean-targets ^{:protect false}
  [:target-path
   [:cljsbuild :builds :app :compiler :output-dir]
   [:cljsbuild :builds :app :compiler :output-to]]

  :resource-paths ["public"]

  :cljsbuild {:builds {:app {:source-paths ["src/cljs"]
                             :compiler {:output-to "public/js/app.js"
                                        :output-dir "public/js/out"
                                        :asset-path   "js/out"
                                        :optimizations :none
                                        :pretty-print  true}}}}

  :sass {:source-paths ["src/sass"]
         :target-path "public/css"
         :output-style :expanded}

  :profiles {:dev {:dependencies [[figwheel "0.5.4-7"]
                                  [org.clojure/tools.nrepl "0.2.12"]
                                  [com.cemerick/piggieback "0.2.1"]]

                   :plugins [[lein-figwheel "0.5.4-7"]]

                   :figwheel {:http-server-root "public"
                              :nrepl-port 7002
                              :nrepl-middleware ["cemerick.piggieback/wrap-cljs-repl"]
                              :css-dirs ["public/css"]}

                   :cljsbuild {:builds {:app {:source-paths ["env/dev/cljs"]
                                              :compiler {:main "emojisweeper.dev"
                                                         :source-map true}}}}}

             :prod {:cljsbuild {:builds {:app
                                         {:source-paths ["env/prod/cljs"]
                                          :compiler
                                          {:optimizations :advanced
                                           :pretty-print false
                                           :source-map "public/js/app.js.map"
                                           :source-map-path "out"}}}}
                    :sass {:output-style :compressed}}})
